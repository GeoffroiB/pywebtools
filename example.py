from pywebinterface import WebServer

from pathlib import Path
from random import randint


def random_number():
    return str(randint(0, 100)).encode()


def main(args):
    root = (Path(".") / "example_root").__str__()
    web_server = WebServer(root)

    web_server.bind("/random_number", random_number)

    with web_server:
        web_server.serve_forever()


if __name__ == "__main__":
    from sys import argv
    main(argv[1:])
