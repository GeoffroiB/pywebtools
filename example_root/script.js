const DEBUG = true;
const server_address = location.hostname + (location.port ? (':' + location.port) : '');
const server_url = new URL("http://" + server_address)

console.log(`Server address: ${server_address}`);

const GET = function(address) {
	return fetch(address)
			.then((response) => {
				if (response.status !== 200) {
					console.log(
						`[GET : ${address}] Encountered a problem (status : ${response.status}`);
					return;
				}

				return response.json(); // .then((data) => { console.log(data); })
			})
			.catch((err) => {
				console.log(`[GET : ${address}] GET Error : ${err}`);
			});
};

const get_random_number = function() {
	const request = new Request(server_address);
	console.log(request);
	fetch(request)
		.then(response => {
			if (response.status !== 200) {
				console.log(response);
				throw new Error('Something went wrong on api server!');
			}

			return response.json();
		});
}

document.getElementById("button_1").addEventListener("click", async () => {
	GET(server_url + "random_number")
		.then((response) => {
			document.getElementById("result").innerHTML = `<b>${response}</b>`;
		})
});