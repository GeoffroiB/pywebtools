from .constants import HTTP_ENCODING, _HTTP_INCOMPLETE_OK_HEADER, HTTP_DEFAULT_PORT
from .messages import Request, Response, ResponseOK, ResponseFileNotFound


def convert_to_http_payload(payload) -> bytes:
    # Argument type normalization
    if not isinstance(payload, bytes):
        if not isinstance(payload, str):
            payload = str(payload).encode(encoding=HTTP_ENCODING)
        else:
            payload = payload.encode(encoding=HTTP_ENCODING)

    return (_HTTP_INCOMPLETE_OK_HEADER
            + str(len(payload)).encode(encoding=HTTP_ENCODING)
            + "\r\n\r\n".encode(encoding=HTTP_ENCODING)
            + payload)


def extract_from_http_payload(payload: bytes) -> str:
    return payload.decode(encoding=HTTP_ENCODING)
