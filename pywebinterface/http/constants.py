HTTP_DEFAULT_PORT: int = 80
HTTP_ENCODING = "utf-8"

HTTP_LINE_DELIM: str = "\r\n"
HTTP_LINE_DELIM_B: bytes = HTTP_LINE_DELIM.encode(HTTP_ENCODING)

_HTTP_INCOMPLETE_OK_HEADER: bytes = "\r\n".join(
    [
        # "HTTP/1.1 200 OK",
        "Cache-Control: private, max-age=0",
        "Connection: keep-alive",
        "Content-Type: text/html",
        "Vary: Origin",
        "Content-Length: "
    ]
).encode(encoding=HTTP_ENCODING)
