from typing import Any, List, Optional

from .constants import HTTP_ENCODING, HTTP_LINE_DELIM, HTTP_LINE_DELIM_B, _HTTP_INCOMPLETE_OK_HEADER


def _split_message(buffer: bytes) -> (str, List[str], str):
    lines: List[str] = buffer.strip().decode(HTTP_ENCODING).split(HTTP_LINE_DELIM)
    index_split: int = lines.index("") if "" in lines else len(lines)-1

    message_params = lines[0].split(' ')
    headers = lines[1:index_split] if index_split else lines[1:]
    body = HTTP_LINE_DELIM.join(lines[index_split+1:]) if index_split else ""

    return message_params, headers, body


class Request:
    def __init__(self, buffer):
        if not isinstance(buffer, bytes):
            if isinstance(buffer, str):
                buffer = buffer.encode(encoding=HTTP_ENCODING)
            else:
                buffer = str(buffer).encode(encoding=HTTP_ENCODING)

        message_params, headers, body = _split_message(buffer)

        self._entries = dict(zip(["Method", "Path", "Protocol"], message_params[:3]))

        for header_line in headers:
            name, value = header_line.split(": ")
            self._entries[name] = value

        self._entries["Body"] = body

    def get(self, entry_name: str) -> Optional[str]:
        try:
            return self._entries[entry_name]
        except KeyError:
            return None

    def __str__(self) -> str:
        return HTTP_LINE_DELIM.join([
            " ".join([self._entries["Method"], self._entries["Path"], self._entries["Protocol"]]),
            HTTP_LINE_DELIM.join([
                    f"{k}: {v}" for k, v in self._entries.items()
                    if k not in ["Method", "Path", "Protocol", "Body"]
            ]),
            self._entries["Body"]
        ])


class Response:
    def __init__(self, status: bytes, data: Any):
        if not isinstance(data, bytes):
            if isinstance(data, str):
                data = data.encode(encoding=HTTP_ENCODING)
            else:
                data = str(data).encode(encoding=HTTP_ENCODING)

        message_params = b"HTTP/1.1 " + status
        headers = _HTTP_INCOMPLETE_OK_HEADER + str(len(data)).encode(HTTP_ENCODING) + HTTP_LINE_DELIM_B

        self._buffer: bytes = HTTP_LINE_DELIM_B.join([message_params, headers, data])

    def to_bytes(self) -> bytes:
        return self._buffer

    def __str__(self) -> str:
        return self._buffer.decode(HTTP_ENCODING)


class ResponseOK(Response):
    def __init__(self, data):
        super().__init__(b"200 OK", data)


class ResponseFileNotFound:
    def __init__(self):
        super().__init__(b"404 Not Found", b"")
