from sys import version_info

_VERSION_VALIDATED = False


def require(version_number: float):
    global _VERSION_VALIDATED
    if not _VERSION_VALIDATED:
        current_version: float = float(f"{version_info.major}.{version_info.minor}")
        assert current_version >= version_number, f"Python's minimum required version was not met.\n" \
                                                  f"  Required minimum version: {version_number:.1f}\n" \
                                                  f"  Current version: {current_version}"
        _VERSION_VALIDATED = True
