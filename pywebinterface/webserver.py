from typing import Any, Dict, Callable, List
from pathlib import Path
import socket
import select

from . import http


class WebServer:
    TIMEOUT = 0.01

    # ==========================================================================
    def __init__(self, root_dir: str, preferred_port: int = http.HTTP_DEFAULT_PORT):
        self._host: str = "127.0.0.1"
        self._port: int = preferred_port
        self._root_dir: Path = Path(root_dir)
        self._socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._virtual_routes: Dict[str, Callable[[], Any]] = {}

    # ==========================================================================
    def __enter__(self) -> "WebServer":
        print("Opening web server")
        self._socket.bind((self._host, self._port))
        self._host, self._port = self._socket.getsockname()
        self._socket.listen(4)
        self._socket.settimeout(WebServer.TIMEOUT)
        self._socket.setblocking(False)
        return self

    # ==========================================================================
    def __exit__(self, exc_type, exc_val, exc_tb):
        if exc_tb:
            print("Closing web server unexpectedly")
            self._socket.close()
            return False
        # else if is recoverable:
        #   (handle)
        #   return True
        else:
            self._socket.close()
            print("Closing web server normally")

    # ==========================================================================
    def serve_forever(self) -> None:
        print(f"Running web server on http://{self._host}:{self._port}\n")

        while True:
            connections = self.accept_new_connections()

            if not connections:
                continue
            else:
                print(f"Connections: {connections}")

            for connection in connections:
                request: http.Request = self._receive(connection)

                response: http.Response = self.handle(request)
                connection.send(response.to_bytes())

                connection.close()

            print("----- LOOP -----", end="\n\n")

    def _receive(self, connection: socket.socket) -> http.Request:
        connection.setblocking(True)
        request: http.Request = http.Request(connection.recv(1024))
        connection.setblocking(False)
        return request

    # ==========================================================================
    def bind(self, route_sub_addr: str, func: Callable[[], bytes]):
        self._virtual_routes[route_sub_addr] = func

    def accept_new_connections(self) -> List[socket.socket]:
        connections = []

        inputs, outputs = [self._socket], []
        readable, writable, exceptional = select.select(inputs, outputs, inputs, WebServer.TIMEOUT)

        for s in readable:
            if s is self._socket:
                conn, addr = self._socket.accept()
                connections.append(conn)

        return connections

    def handle(self, request: http.Request):
        requested_path = request.get("Path")

        if requested_path.startswith("/"):
            if requested_path == "/":
                file_route = self._root_dir / "index.html"
            else:
                file_route = self._root_dir / requested_path[1:]

            if file_route.exists():
                with open(file_route, mode="rb") as f:
                    return http.ResponseOK(data=f.read())
            elif requested_path in self._virtual_routes:
                return http.ResponseOK(data=self._virtual_routes[requested_path]())

        print(f"Could not resolve '{requested_path}'")
        return http.ResponseFileNotFound()
